<?php
use yii\helpers\Html;
use yii\widgets\ListView;


$this->title = 'Planetas Templados';
?>
<div class="row">
    <div class=" body-content" style="color:#4A4A4A">
        <h1 class="encabezadoObras">Planetas Templados</h1>

            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_resultado-climatemplado',
                'layout' => " \n {items} \n\n{pager}",
                'itemOptions' => [
                    'class' => 'list-view-planetas',
                ],
            ]);
            ?>

    </div>
</div>