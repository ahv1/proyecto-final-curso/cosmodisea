<?php
use yii\grid\GridView;
?>

<div class="fondo">
    <div class="jumbotron">
        <h2><?=$titulo?></h2>
    
        <p class="lead"><?= $enunciado ?></p>
    </div>

    <?= GridView::widget([
        'dataProvider' => $mostrarPlanetas,
        'columns'=>$campos,
        
        ]); 
    ?>
</div>