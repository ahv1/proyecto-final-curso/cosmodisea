<?php
use yii\helpers\Html;
use yii\widgets\ListView;


$this->title = 'Planetas Calidos';
?>
<div class="row">
    <div class=" body-content" style="color:#4A4A4A">
        <h1 class="encabezadoObras">Planetas Calidos</h1>

            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_resultado-clima',
                'layout' => " \n {items} \n\n{pager}",
                'itemOptions' => [
                    'class' => 'list-view-planetas',
                ],
            ]);
            ?>

    </div>
</div>