<?php

use app\models\Planetas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Destinos';
$this->params['breadcrumbs'][] = $this->title;
?>




<!--ETIQUETASSSSSSS-->
    
    <div class="container-fluid bg-dark mb-auto">
        <div class="row">
            <div class="col-md text-white text-center">
                <h1 class="display-4 text-center margenh">Los mejores destinos a tu alcance</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md text-white text-center">
                <p class="lead text-white text-center margenparrafo">Aqui podras echarle un vistazo a todos los destinos disponibles para que tengas tus mejores vacaciones intergalacticas.</p>
            </div>
        </div>
    </div>
    
    <div class="body-content">

        <div class="body">

    <!--inicio de la primera fila-->

            <div class="row">

                <!--boton de consulta-->

                <div class="card" style="width: 18rem;">

                    <img src="<?= Yii::getAlias('@web')?>/img/DESTINOS/MARTE.jpg" class="card-img-top" alt="etapa valla">
                    <div class="card-body">

                        <h5 class="card-title">Marte</h5>
                        <p class="card-text">Visita el planeta rojo</p>
                            <p class="card-text"></p>
                        <p>
                            <!-- BOTON POPUP -->


                                <!-- Modal -->
                               <!-- Large modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Recorrido</button>

                                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <?= Html::img('@web/img/grafico v.jpg', ['alt' => 'Grafico Valladolid']) ?>
                                    </div>
                                  </div>
                                </div>

                            <!-- FIN BOTON POPUP -->
                            
                            <?= Html::a('INFO', ['site/mostrar'],['class'=>'btn btn-warning'])?>

                        </p>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
    
    <!--END ETIQUETASSSSSSS-->