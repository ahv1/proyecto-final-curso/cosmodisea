<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Planetas;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    /**LISTVIEW
    public function actionMostrarPlanetas(){
      
        $dataProvider = new ActiveDataProvider([
            'query' => Planetas::find(),
        ]);
        return $this->render("planetas",[
            "dataProvider" =>$dataProvider,
           
        ]);
    }
     * 
     */
    
    /**EMPIEZAN QUERYS PARA LOS DATOS DE LOS PLANETAS**/
    
    
    public function actionMostrar(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Venus"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar1(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Marte"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar2(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Orcus"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar3(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Quaoar"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar4(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Varuna"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar5(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Ixion"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar6(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Huya"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar7(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Albión"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar8(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Salacia"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar9(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Gonggong"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar10(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Veesta"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar11(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Palas"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar12(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Hygiea"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar13(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Juno"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar14(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Astrea"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar15(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Iris"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar16(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Flora"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar17(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Kepler-186f"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar18(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Proxima Centauri"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar19(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "TRAPPIST-1e"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar20(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Osiris"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar21(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "WASP-12b"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar22(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Urano"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar23(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "55 Cancri e"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar24(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Plutón"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar25(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Ceres"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar26(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Makemake"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar27(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Haumea"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar28(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Eris"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    public function actionMostrar29(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, medioambiente, misiones FROM planetas WHERE nombre= "Sedna"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'medioambiente', 'misiones'],
            "titulo"=>"Estos son sus planetas",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);

    }
    
    /**TERMINAN QUERYS PARA LOS DATOS DE LOS PLANETAS**/
    
    /**EMPIEZAN LAS CONSULTAS PARA FILTRAR PLANETAS**/
    
    
    /**public function actionClimaCalido(){
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, habitabilidad, misiones FROM planetas WHERE medioambiente= "Caliente"',
        ]);
        
        return $this->render("resultado",[
            "mostrarPlanetas"=>$dataProvider,
            "campos"=>['nombre', 'habitabilidad', 'misiones'],
            "titulo"=>"Estos son los planetas de clima calido",
            "enunciado"=>"Aqui se muestran los datos de cada planeta",
        ]);
        
    }**/
    
    
    
    
    //**CONSULTAS DE FILTRACION DE PLANETAS**/
    
        //**CONSULTAS DE FILTRACION DE CLIMAS**/
    
    public function actionClimacalido()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, habitabilidad, misiones')
            ->where(['medioambiente' => 'Caliente']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-clima', [
        'dataProvider' => $dataProvider,
    ]);
}

    public function actionClimafrio()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, habitabilidad, misiones')
            ->where(['medioambiente' => 'Frio']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-climafrio', [
        'dataProvider' => $dataProvider,
    ]);
}

    public function actionClimatemplado()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, habitabilidad, misiones')
            ->where(['medioambiente' => 'Templado']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-climatemplado', [
        'dataProvider' => $dataProvider,
    ]);
}

        //**CONSULTAS DE FILTRACION DE ESTADOS**/

    public function actionEstadohab()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, medioambiente, misiones')
            ->where(['habitabilidad' => 'Habitable']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-hab', [
        'dataProvider' => $dataProvider,
    ]);
}

    public function actionEstadoen()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, medioambiente, misiones')
            ->where(['habitabilidad' => 'En proceso de habitabilidad']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-en', [
        'dataProvider' => $dataProvider,
    ]);
}

    public function actionEstadono()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, medioambiente, misiones')
            ->where(['habitabilidad' => 'No habitable']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-no', [
        'dataProvider' => $dataProvider,
    ]);
}
    

   //**CONSULTAS DE FILTRACION DE PUERTOS**/

        //**CONSULTAS DE FILTRACION DE ZONA**/

    public function actionZonaeli()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Puertos::find()
            ->select('nombre, tipo_astro, naves_permitidas, comercio')
            ->where(['zona_orbita' => 'Elíptica']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-zonaeli', [
        'dataProvider' => $dataProvider,
    ]);
}

    public function actionZonageo()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, tipo_astro, naves_permitidas, comercio')
            ->where(['zona_orbita' => 'Geoestacionaria']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-clima', [
        'dataProvider' => $dataProvider,
    ]);
}

    public function actionZonabaja()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, tipo_astro, naves_permitidas, comercio')
            ->where(['zona_orbita' => 'Baja']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-clima', [
        'dataProvider' => $dataProvider,
    ]);
}




        //**CONSULTAS DE FILTRACION DE NAVES**/

    public function actionNavesgra()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, tipo_astro, zona_orbita, comercio')
            ->where(['naves_permitidas' => 'Grande']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-clima', [
        'dataProvider' => $dataProvider,
    ]);
}

    public function actionNavesmid()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, tipo_astro, zona_orbita, comercio')
            ->where(['naves_permitidas' => 'Mediana']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-clima', [
        'dataProvider' => $dataProvider,
    ]);
}

    public function actionNavespeq()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, tipo_astro, zona_orbita, comercio')
            ->where(['naves_permitidas' => 'Pequeña']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-clima', [
        'dataProvider' => $dataProvider,
    ]);
}
        //**CONSULTAS DE FILTRACION DE COMERCIO**/

    public function actionComercioest()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Planetas::find()
            ->select('nombre, tipo_astro, naves_permitidas, comercio')
            ->where(['medioambiente' => 'Caliente']),
        'pagination' => [
            'pageSize' => 10,
        ],
    ]);

    return $this->render('resultado-clima', [
        'dataProvider' => $dataProvider,
    ]);
}
        
        
     
    
    
        
    
    

    
}


